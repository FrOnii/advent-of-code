import sys

nbmax = {'green':13,'red':12,'blue':14}
def run1(input):
	total = 0
	with open("input.txt") as fp:
		Lines = fp.readlines()
		for line in Lines:
			id_game,rest = line.split(':')
			id_game = int(id_game[4:]) 
			balls_set = rest.split(';')
			valid = True
			for balls in balls_set:
				nbball = {'green':0,'red':0,'blue':0}
				balls = balls[1:].replace(',','').replace('\n','')
				splitballs = balls.split(' ')
				for i in range(0,len(splitballs)//2):
					nbball[splitballs[2*i+1]]=int(splitballs[2*i])
				valid = valid and nbmax['green'] >= nbball['green'] and nbmax['blue'] >= nbball['blue'] and nbmax['red'] >= nbball['red']
			if valid: 
				total += id_game
	return total

def run2(input):

	total = 0
	with open("input.txt") as fp:
		Lines = fp.readlines()
		for line in Lines:
			id_game,rest = line.split(':')
			id_game = int(id_game[4:]) 
			balls_set = rest.split(';')
			valid = True
			minnbball = {'green':0,'red':0,'blue':0}
			for balls in balls_set:
				nbball = {'green':0,'red':0,'blue':0}
				balls = balls[1:].replace(',','').replace('\n','')
				splitballs = balls.split(' ')
				for i in range(0,len(splitballs)//2):
					nbball[splitballs[2*i+1]]=int(splitballs[2*i])
				minnbball['green']=max(minnbball['green'],nbball['green'])
				minnbball['blue']=max(minnbball['blue'],nbball['blue'])
				minnbball['red']=max(minnbball['red'],nbball['red'])
			total +=	minnbball['green']*minnbball['blue']*minnbball['red']
	return total

if sys.argv[1] =='1':
	print(run1(input))
elif sys.argv[1] == '2':
	print(run2(input))
