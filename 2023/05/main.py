import sys
from pprint import pprint 

def run1():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		inp = [int(i) for i in Lines[0].split(' ')[1:]]
		Lines = Lines[1:]+['map'] #Pour tout mettre dans inp une fois à la fin
		outp = []
		for line in Lines:
			if len(line)<3:
				pass
			elif 'map' in line:
				inp = outp.copy() + inp.copy() 
				outp = []
			else:
				[d,s,l] = [int(i) for i in line.split(' ')]
				j=0
				for i in inp.copy():
					j+=1
					if i >= s and i < s + l: 
						outp.append(d+i-s)
						inp.remove(i)
						print('q')
						pass
		return min(inp)

def run2():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		inp = []
		sp = Lines[0].split(' ')[1:]
		m = 100000000000000000000000
		mapss= []
		tmp =[]
		seeds = [] #[(start,end),(s,e),....]
		for s in range(0,len(sp),2):
			seeds.append((int(sp[s]),int(sp[s])+int(sp[s+1])))
		Lines = Lines[1:] #Pour tout mettre dans inp une fois à la fin 
		for line in Lines:
			if len(line)<3:
				pass
			elif 'map' in line:
				mapss.append(tmp.copy())
				tmp = []
			else:
				tmp.append(list(map(int,line.split(' '))))
		mapss.append(tmp.copy())
		for maps in mapss:
			new = []
			while seeds:
				start, end = seeds.pop()
				for d,s,l in maps:
					os = max(start,s) #overlap start
					oe = min(end, s+l) #overlap end
					if os < oe: #s'il y a overlap
						new.append((os-s+d,oe-s+d))
						if os > start: #s'il reste de l'intervale avant
							seeds.append((start,os))
						if oe < end: #s'il reste de l'intervale après
							seeds.append((oe,end))
						break
				else: #si le for a pas break
					new.append((start,end))
			seeds = new
	return sorted(seeds)



if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
