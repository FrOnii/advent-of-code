import sys
from pprint import pprint 
from math import sqrt

cardtype=['A','K','Q','J','T','9','8', '7', '6','5','4','3','2','1']
handtype=['HC','1P','2P','3K','FH','4K','5K']

ct_to_alphabet={'A':'n','K':'m','Q':'l','J':'k','T':'j','9':'i','8':'h', '7':'g', '6':'f' ,'5':'e','4':'d','3':'c','2':'b','1':'a'}
ct_to_alphabet2={'A':'n','K':'m','Q':'l','T':'k','9':'j','8':'i', '7':'h', '6':'g' ,'5':'f','4':'e','3':'d','2':'c','1':'b','J':'a'}

def checktype(hand):
	nbcartesdiff=0
	nbfirstcard = 0
	for c in cardtype:
		if c in hand:
			nbcartesdiff+=1
	match nbcartesdiff:
		case 1:
			return '5K'
		case 2:
			for c in cardtype:
				if hand.count(c) == 4 or hand.count(c) == 1:
					return '4K'
				elif hand.count(c) == 2 or hand.count(c) == 3:
					return 'FH'
		case 3:
			for c in cardtype:
				if hand.count(c) == 3:
					return '3K'
			return  '2P'
		case 4:
			return '1P'
		case 5:
			return 'HC'

def sort(hand):
	temp = ''
	for c in hand[0]:
		temp += ct_to_alphabet[c]
	return temp

def sort2(hand):
	temp = ''
	for c in hand[0]:
		temp += ct_to_alphabet2[c]
	return temp

def run1():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		result = {'HC':[],'1P':[],'2P':[],'FH':[],'3K':[],'4K':[],'5K':[]}
		for l in Lines:
			hand,bid = l.split(' ')
			result[checktype(hand)].append((hand,int(bid)))
		resultsorted = []
		for ht in handtype:
			result[ht].sort(key=sort)
			resultsorted = resultsorted + result[ht]
		total = 0
		for i in range(len(resultsorted)):
			total += (i+1)*resultsorted[i][1]
		return total


def run2():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		result = {'HC':[],'1P':[],'2P':[],'FH':[],'3K':[],'4K':[],'5K':[]}
		for l in Lines:
			hand,bid = l.split(' ')
			if 'J' in hand: 
				id_htype = 0
				for c in cardtype:
					t = hand.replace('J',c) 
					id_htype = max(id_htype,handtype.index(checktype(t)))	
				result[handtype[id_htype]].append((hand,int(bid)))
			else:
				result[checktype(hand)].append((hand,int(bid)))
		resultsorted = []
		for ht in handtype:
			result[ht].sort(key=sort2)
			resultsorted = resultsorted + result[ht]
		total = 0
		for i in range(len(resultsorted)):
			total += (i+1)*resultsorted[i][1]
		return total

if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
