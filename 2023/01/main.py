import sys

nb = [[1,'one','eno'], 
	  [2, 'two', 'owt'], 
	  [3, 'three', 'eerht'], 
	  [4, 'four', 'ruof'], 
	  [5, 'five', 'evif'], 
	  [6, 'six', 'xis'], 
	  [7, 'seven', 'neves'], 
	  [8, 'eight','thgie'],
	  [9,'nine','enin']]

def run1(input):
	with open("input.txt") as fp:
		total = 0
		Lines = fp.readlines()
		for line in Lines:
			i = 0
			while not line[i].isnumeric():
				i += 1
			total += int(line[i])*10
			i = len(line)-1
			while not line[i].isnumeric():
				i -= 1
			total += int(line[i])
	return total


def run2(input):
	with open("input.txt") as fp:
		total = 0
		Lines = fp.readlines()
		for line in Lines:
			min = 100000000000
			min_nb = 1 
			for n in nb:
				if str(n[0]) in line:
					index = line.index(str(n[0]))
					if index < min:
						min_nb = n[0]
						min = index
				if n[1] in line:
					index = line.index(n[1])
					if index < min:
						min_nb = n[0]
						min = index
			total += min_nb*10
			line = line[::-1]
			min = 100000000000
			min_nb = 1 
			for n in nb:
				if str(n[0]) in line:
					index = line.index(str(n[0]))
					if index < min:
						min_nb = n[0]
						min = index
				if n[2] in line:
					index = line.index(n[2])
					if index < min:
						min_nb = n[0]
						min = index
			total += min_nb

	return total

if sys.argv[1] =='1':
	print(run1(input))
elif sys.argv[1] == '2':
	print(run2(input))
