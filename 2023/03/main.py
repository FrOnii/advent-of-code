import sys
from pprint import pprint 

def voisins(arr,x,y): 
	leny = len(arr)
	lenx = len(arr[0])
	voisins = []
	for i in range(max(y-1,0),min(y+2,leny)):
		for j in range(max(x-1,0),min(x+2,lenx)):
			if j != x or i != y:
				voisins.append(arr[i][j])
	return voisins 

def check_signe(input,x,y):
	ret = False
	for v in voisins(input,x,y):
		ret = ret or not (v=='.' or v.isnumeric())
	return ret 

def run1():
	total = 0
	signe = False
	nb = ''
	with open("input.txt") as fp:
		input = fp.read().split('\n')[:-1]
		for y in range(len(input)):
			for x in range(len(input[y])):
				char = input[y][x]
				if char.isnumeric():
					nb += char
					signe = signe or check_signe(input,x,y)
				else:
					if signe:
						total += int(nb)
					signe = False
					nb = ''
	return total

arr = []
def get_number(x,y,dir):
	if y<0 or y>len(arr) or x < 0 or x >= len(arr[y]) or arr[y][x]=='.' or not arr[y][x].isnumeric():
		return ''
	nb = arr[y][x]
	arr[y] = arr[y][:x]+ "."+ arr[y][x+1:] 
	if dir == -1:
		return get_number(x-1,y,-1) + nb
	return nb +get_number(x+1,y,1) 

def get_ratio(x,y):
	leny = len(arr)
	lenx = len(arr[0])
	nb = []
	for i in range(max(y-1,0),min(y+2,leny)):
		for j in range(max(x-1,0),min(x+2,lenx)):
			if arr[i][j].isnumeric():
				nb.append(get_number(j-1,i,-1)+arr[i][j]+get_number(j+1,i,1))
	if len(nb)==2:
		return int(nb[0])*int(nb[1])
	return 0

def run2():
	global arr
	total = 0
	nb = 0
	with open("input.txt") as fp:
		arr = fp.read().split('\n')[:-1]
		for y in range(len(arr)):
			for x in range(len(arr[y])):
				char = arr[y][x]
				if char == '*':
					total += get_ratio(x,y)
	return total

if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
