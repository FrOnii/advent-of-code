import sys
from pprint import pprint 
from math import lcm

def run1():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		pattern = Lines[0][:-1]
		map = {}
		for line in Lines[2:]:
			map[line[0:3]]=(line[7:10],line[12:15])
		i = 0
		place = 'AAA'
		while place != 'ZZZ':
			for c in pattern:
				i+=1
				if c == 'L':
					place = map[place][0]
				else:
					place = map[place][1]
		return i

def run2():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		pattern = Lines[0][:-1]
		map = {}
		start = []
		for line in Lines[2:]:
			if line[2]=='A':
				start.append(line[0:3])
			map[line[0:3]]=(line[7:10],line[12:15])
		steps=[]
		for s in start:
			i = 0
			place = s
			while place[2] != 'Z':
				for c in pattern:
					i+=1
					if c == 'L':
						place = map[place][0]
					else:
						place = map[place][1]
			steps.append(i)
		ppcm = 1
		for s in steps: 
			ppcm = lcm(ppcm,s)
		return ppcm


if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
