import sys
from pprint import pprint 
from math import sqrt
def run1():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		time = [int(i) for i in Lines[0].split(' ')[1:] if i]
		distance = [int(i) for i in Lines[1].split(' ')[1:] if i]
		#f(x)=x(t-x)-d=-x²+tx-d
		#Delta = t²-4d
		#Racine : (-t+sqrt(Delta))/-2 (-t-sqrt(Delta))/-2
		total = 1
		for i in range(len(time)):
			t = time[i]
			d = distance[i]
			delta = t*t-4*d  
			x1 = (-t+sqrt(delta))/-2
			x2 = (-t-sqrt(delta))/-2
			diff = 0
			if x1%1==0: diff = 1 #Si x1 est entier 
			total*=(int(x2)-int(x1)-diff)
		return total

def run2():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		t = int(Lines[0].split(':')[1].replace(' ',''))
		d = int(Lines[1].split(':')[1].replace(' ',''))
		#f(x)=x(t-x)-d=-x²+tx-d
		#Delta = t²-4d
		#Racine : (-t+sqrt(Delta))/-2 (-t-sqrt(Delta))/-2
		delta = t*t-4*d  
		x1 = (-t+sqrt(delta))/-2
		x2 = (-t-sqrt(delta))/-2
		diff = 0
		if x1%1==0: diff = 1 #Si x1 est entier 
		return (int(x2)-int(x1)-diff)




if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
