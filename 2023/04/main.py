import sys
from pprint import pprint 

def run1():
	total = 0
	with open("input.txt") as fp:
		Lines = fp.readlines()
		for line in Lines:
			line = line.replace('  ',' ')
			_, line = line.split(': ')
			win, nb = line.split(' | ')
			win = win.split(' ')
			nb = nb.split(' ')
			i = 0
			for w in win:
				if w.isnumeric():
					for n in nb:
						if int(n) == int(w):
							i+=1
			if i > 0:
				total+=int(2**(i-1))
	return total


def run2():
	total = 0
	wins=[]
	nbs=[]
	mult=[]
	with open("input.txt") as fp:
		Lines = fp.readlines()
		for line in Lines:
			line = line.replace('  ',' ')
			_, line = line.split(': ')
			win, nb = line.split(' | ')
			win = win.split(' ')
			nb = nb.split(' ')
			wins.append(win)
			nbs.append(nb)
			mult.append(1)
	scratchnb = 1
	i = 0
	while i < len(mult) and mult[i]>0:
		nb = 0
		for w in wins[i]:
			if w.isnumeric():
				for n in nbs[i]:
					if int(n) == int(w):
						nb+=1
		for j in range(i+1,nb+i+1):
			mult[j]+=(mult[i])
		i+=1
	return sum(mult)

if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
