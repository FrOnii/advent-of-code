import sys
from pprint import pprint 
from math import lcm


def oasis(arr):
	for i in arr:
		if i != 0:
			break
	else:
		return 0
	t = []
	for i in range(1,len(arr)):
		t.append(arr[i]-arr[i-1])
	return arr[-1] + oasis(t)

def run1():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		total = 0
		for line in Lines: 
			total += oasis([int(i) for i in line.split(' ')])
		return total


def oasis2(arr):
	for i in arr:
		if i != 0:
			break
	else:
		return 0	
	t = []
	for i in range(1,len(arr)):
		t.append(arr[i]-arr[i-1])
	return arr[0]-oasis2(t)

def run2():
	with open("input.txt") as fp:
		Lines = fp.readlines()
		total = 0
		for line in Lines: 
			total += oasis2([int(i) for i in line.split(' ')])
		return total



if sys.argv[1] =='1':
	print(run1())
elif sys.argv[1] == '2':
	print(run2())
