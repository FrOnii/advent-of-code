f = open("input.txt", "r")
vowels = ['a','e','i','o','u']
ban = ['ab', 'cd', 'pq','xy']
total = 0
for line in f:
    nbvowels = 0
    i=0
    while nbvowels < 3 and i < 5 :
        nbvowels += line.count(vowels[i])
        i += 1
    if nbvowels < 3:
        continue
    double = False
    for i in range(len(line)-1):
        if line[i]==line[i+1]:
            double = True
            break
    if not double :
        continue

    banable = False
    for b in ban:
        if b in line:
            banable = True
            break
    if banable:
        continue

    total += 1

print(total)
