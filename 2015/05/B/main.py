f = open("input.txt", "r")
total = 0
for line in f:
    doublepair= False
    cond2 = False
    for i in range(len(line)-2):
        doublepair = doublepair or (line[i]+line[i+1] in line[i+2:])
        cond2 = cond2 or (line[i]==line[i+2])
    if doublepair and cond2:
        total += 1

print(total)
