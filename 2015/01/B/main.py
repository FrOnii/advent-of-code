f = open("input.txt", "r").read()

floor = 0
i = 0
while floor >= 0:
    if f[i] == '(':
        floor += 1
    else:
        floor -= 1
    i += 1

print(i)
