f = open("input.txt", "r")

total = 0
for line in f:
    dim = line.replace("\n","").split("x")
    l,w,h = int(dim[0]),int(dim[1]),int(dim[2])
    aire1=2*h*l
    aire2=2*w*h
    aire3=2*l*w
    total += aire1 + aire2 + aire3 + min(aire1,aire2,aire3)/2

print(int(total))