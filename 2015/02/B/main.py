f = open("input.txt", "r")

total = 0
for line in f:
    dim = line.replace("\n","").split("x")
    l,w,h = int(dim[0]),int(dim[1]),int(dim[2])
    perimetre = [2*l+2*w,2*w+2*h,2*l+2*h]
    total += min(perimetre) + l*w*h
print(int(total))