f = open("input.txt", "r").readline()
maisons = [[0,0]]
coordonnes = [0,0]
for c in f[0::2]:
    y=0
    x=0
    if c =='^':
        y=-1
    elif c == 'v':
        y=1
    elif c == '>':
        x=1
    elif c== '<':
        x=-1
    coordonnes=[coordonnes[0]+x,coordonnes[1]+y]
    if coordonnes not in maisons:
        maisons.append(coordonnes)

coordonnes = [0,0]
for c in f[1::2]:
    y=0
    x=0
    if c =='^':
        y=-1
    elif c == 'v':
        y=1
    elif c == '>':
        x=1
    elif c== '<':
        x=-1
    coordonnes=[coordonnes[0]+x,coordonnes[1]+y]
    if coordonnes not in maisons:
        maisons.append(coordonnes)

print(len(maisons))