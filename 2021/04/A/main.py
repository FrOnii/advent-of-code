from pprint import pprint

def check(checkTable,x,y):
    checkX = True
    checkY = True
    checkDiag = False
    for i in range(5):
        checkX = checkX and checkTable[y][i]
    for i in range(5):
        checkY = checkY and checkTable[i][x]
    if x==y:
        for i in range(5):
            checkDiag = checkDiag and checkTable[i][i]
    elif x+y==4:
        for i in range(5):
            checkDiag = checkDiag and checkTable[i][4-i]
    return checkX or checkY or checkDiag
        
f = open("input.txt", "r")
numbers = f.readline().replace("\n","").split(",")
tables = []
tablesCheck = []
for line in f:
    temp = []
    defaultCheck = [[False,False,False,False,False],[False,False,False,False,False],[False,False,False,False,False],[False,False,False,False,False],[False,False,False,False,False]]
    for i in range(5):
        temp = temp + [int(x) for x in f.readline().replace("\n","").split(" ") if x]
    tablesCheck.append(defaultCheck)
    tables.append(temp)

stop = False
win = -1
noNumber = 0
while not stop :
    numb = int(numbers[noNumber])
    for i in range(len(tables)):
        if numb in tables[i]:
            ind = tables[i].index(numb)
            x = ind%5
            y = ind//5
            tablesCheck[i][y][x]= True
            if check(tablesCheck[i],x,y):
                win = i
                stop = True
                break
    noNumber = noNumber + 1

varFalse = 0
for a in range(5):
    for b in range(5):
        if not tablesCheck[win][a][b]:
            varFalse = varFalse+tables[win][a*5+b]

print(numb*varFalse)
    
    
