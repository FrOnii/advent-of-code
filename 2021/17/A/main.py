f = open("input.txt", "r").readline()
_, intervalx,intervaly = f.split("=")
intervalx = intervalx.split(",")[0].split(".")
intervalx = range(int(intervalx[0]), int(intervalx[-1])+1)
intervaly = intervaly.split(".")
intervaly = range( int(intervaly[0]), int(intervaly[-1])+1)

def tir(vx,vy,interx,intery):
    x, y = 0, 0
    maxy = y
    cont = True
    while cont:
        x, y = vx + x, vy + y
        vy -= 1
        if vx > 0:
            vx -= 1
        elif vx < 0:
            vx += 1
        maxy = max(maxy, y)
        if x in interx and y in intery:
            return maxy
        elif x > max(interx) or y < min(intery):
            return None

# vy = abs(min(intervaly))-1
# ex : vy = 9, min = -10
# y = 9,17,24,30,35,39,42,44,45,45,44,42,39,35,30,24,17,9,0,-10=min
# A chaque fois, y atteint le max en sum(1,vy), touche le 0 plus tard,
# et on veut qu'il atteigne l'intervale, le minimum marche, donc -abs(min) après avoir atteint le 0
y = abs(min(intervaly))-1
print(y*(y+1)//2)