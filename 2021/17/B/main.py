f = open("input.txt", "r").readline()
_, intervalx,intervaly = f.split("=")
intervalx = intervalx.split(",")[0].split(".")
intervalx = range(int(intervalx[0]), int(intervalx[-1])+1)
intervaly = intervaly.split(".")
intervaly = range( int(intervaly[0]), int(intervaly[-1])+1)

def tir(vx,vy,interx,intery):
    x, y = 0, 0
    maxy = y
    cont = True
    while cont:
        x, y = vx + x, vy + y
        vy -= 1
        if vx > 0:
            vx -= 1
        elif vx < 0:
            vx += 1
        maxy = max(maxy, y)
        if x in interx and y in intery:
            return maxy
        elif x > max(interx) or y < min(intery):
            return None
abs_min_y=abs(min(intervaly))
total = 0
for vx in range(max(intervalx)+1):
    for vy in range(-abs_min_y,abs_min_y):
        if tir(vx,vy,intervalx,intervaly) is not None:
            total+=1
print(total)