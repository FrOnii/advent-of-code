f = open("input.txt", "r")
NB_TURN = 100


def check(arr,x,y,flash):
    ret = arr
    retflash = flash
    if ret[y][x] > 9 :
        ret[y][x] = 0
        retflash += 1
        deltavoisinx = [x]
        deltavoisiny = [y]
        if x > 0:
            deltavoisinx.append(x-1)
        if x < len(arr[y])-1:
            deltavoisinx.append(x+1)
        if y > 0:
            deltavoisiny.append(y-1)
        if y < len(arr[y])-1:
            deltavoisiny.append(y+1)
        for xtemp in deltavoisinx:
            for ytemp in deltavoisiny:
                if not (ytemp == y and xtemp == x) and ret[ytemp][xtemp] < 10 and ret[ytemp][xtemp] > 0:
                    ret[ytemp][xtemp] += 1
                    ret,retflash = check(ret,xtemp,ytemp,retflash)
        ret[y][x]=0
    return ret, retflash

inp = []
for line in f:
    inp.append([int(x) for x in line.replace("\n","")])
flash = 0

for i in range(NB_TURN):
    for y in range(len(inp)):
        for x in range(len(inp[y])):
            inp[y][x] += 1
    for y in range(len(inp)):
        for x in range(len(inp[y])):
            inp, flash = check(inp,x,y,flash)

print(flash)