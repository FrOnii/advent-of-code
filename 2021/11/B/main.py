from PIL import Image, ImageDraw

f = open("input.txt", "r")
NB_TURN = 100

GRID_SIZE=10
TILE_SIZE=20
IMAGES = []


def update(arr):
    out = Image.new("RGB", (TILE_SIZE*GRID_SIZE, TILE_SIZE*GRID_SIZE), (0, 0, 0))
    img = ImageDraw.Draw(out)
    for y in range(GRID_SIZE):
        for x in range(GRID_SIZE):
            color = (int(255*arr[y][x]/10),int(255*arr[y][x]/10),int(255*arr[y][x]/10))
            shape = [(x * TILE_SIZE, y * TILE_SIZE), ((x + 1) * TILE_SIZE, (y + 1) * TILE_SIZE)]
            img.rectangle(shape, fill=color)
    IMAGES.append(out)

def check(arr,x,y,flash):
    ret = arr
    retflash = flash
    if ret[y][x] > 9 :
        ret[y][x] = 0
        retflash += 1
        deltavoisinx = [x]
        deltavoisiny = [y]
        if x > 0:
            deltavoisinx.append(x-1)
        if x < len(arr[y])-1:
            deltavoisinx.append(x+1)
        if y > 0:
            deltavoisiny.append(y-1)
        if y < len(arr[y])-1:
            deltavoisiny.append(y+1)
        for xtemp in deltavoisinx:
            for ytemp in deltavoisiny:
                if not (ytemp == y and xtemp == x) and ret[ytemp][xtemp] < 10 and ret[ytemp][xtemp] > 0:
                    ret[ytemp][xtemp] += 1
                    ret,retflash = check(ret,xtemp,ytemp,retflash)
        update(ret)
        ret[y][x]=0
    return ret, retflash

inp = []
for line in f:
    inp.append([int(x) for x in line.replace("\n","")])
flash = 0

step = 0
stop = False
while not stop:
    for y in range(len(inp)):
        for x in range(len(inp[y])):
            inp[y][x] += 1
    for y in range(len(inp)):
        for x in range(len(inp[y])):
            inp, flash = check(inp,x,y,flash)
    stop = True
    for y in range(len(inp)):
        for x in range(len(inp[y])):
            stop = stop and inp[y][x]==0
    step +=1

print(step)
IMAGES[0].save('out.gif',
               save_all=True, append_images=IMAGES[1:], optimize=False, duration=1, loop=0,
               interlace=True, include_color_table=True,disposal=0)