import heapq

f = open("input.txt", "r")



def adjacent(p,arr):
    maxX = len(arr[0])
    maxY = len(arr)
    x,y = p
    for i,j in [(x-1,y),(x+1,y),(x,y-1),(x,y+1)]:
        if 0 <= i < maxX and 0 <= j < maxY:
            yield (i, j), arr[i][j]

def dijkstra(inp, start, end):
    dist = {start: 0}
    zones_prioritaires = [(0, start)]

    heapq.heapify(zones_prioritaires)

    while zones_prioritaires:
        cout, sommet = heapq.heappop(zones_prioritaires)
        if cout <= dist[sommet]:
            for s, poids in adjacent(sommet,inp):
                alt = cout + poids
                if s not in dist or alt < dist[s]:
                    dist[s] = alt
                    heapq.heappush(zones_prioritaires, (alt, s))

    return dist[end]


inp = []
for line in f:
    inp.append([int(x) for x in line.replace("\n","")])

maxX = len(inp[0])
maxY = len(inp[1])

print(dijkstra(inp,(0,0),(maxX-1,maxY-1)))
