from pprint import pprint

#number | encod | size
# 0 | abcefg | 6
# 1 | cf | 2
# 2 | acdeg | 5
# 3 | acdfg | 5
# 4 | bcdf | 4
# 5 | abdfg | 5
# 6 | abdefg | 6
# 7 | acf | 3
# 8 | abcdefg | 7
# 9 | abcdfg | 6
f = open("input.txt", "r")
total = 0
for line in f:
    encoded = ["","","","","","","","","abcdefg",""]
    size5 = []
    size6 = []
    signal = line.replace("\n","").split(" | ")
    output = signal[1].split(" ")
    encod = signal[0].split(" ") + output
    for t in encod:
        l = len(t)
        if l == 2:
            encoded[1]="".join(sorted(t))
        elif l == 4:
            encoded[4]="".join(sorted(t))
        elif l == 3:
            encoded[7]="".join(sorted(t))
        elif l == 6:
            if t not in size6:
                size6.append("".join(sorted(t)))
        elif l == 5:
            if t not in size5:
                size5.append("".join(sorted(t)))

    #Taille 6 : le 0 a pas de d, le 6 de c, le 9 de e
    # Pour décoder le 0 : on check si tous les caractères du 4 sont dedans, et il manquera le d pour le 0
    # Pour décoder le 6 : on check si tous les caractères du 1 sont dedans, et il manquera le c pour le 6
    for s in size6:
        if encoded[1][0] not in s or encoded[1][1] not in s:
            encoded[6]=s
        else:
            test4 = False
            for q in encoded[4]:
                if q not in s:
                    test4=True
            if test4:
                encoded[0]=s
            else:
                encoded[9]=s
                
    #Taille 5 : 3 a tous les éléments du 1, le 2 a un élément qui n'est pas dans le 6
    for s in size5:
        if encoded[1][0] in s and encoded[1][1] in s:
            encoded[3]=s
        else:
            test2 = False
            for c in s:
                if c not in encoded[6]:
                    test2=True
            if test2:
                encoded[2]=s
            else:
                encoded[5]=s

    temp=""
    for out in output:
        temp+=str(encoded.index("".join(sorted(out))))
    total+=int(temp)
print(total)
