from pprint import pprint

#number | encod | size
# 0 | abcefg | 6
# 1 | cf | 2
# 2 | acdeg | 5
# 3 | acdfg | 5
# 4 | bcdf | 4
# 5 | abdfg | 5
# 6 | abdefg | 6
# 7 | acf | 3
# 8 | abcdefg | 7
# 9 | abcdfg | 6
f = open("input.txt", "r")
total = 0
for line in f:
    size =[len(x) for x in line.replace("\n","").split(" | ")[1].split(" ")]
    for s in size:
        if s in [2,4,3,7] : #si la taille correspond à 1 4 7 ou 8
            total += 1
print(total)
