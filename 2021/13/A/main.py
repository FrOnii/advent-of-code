from pprint import pprint
from PIL import Image, ImageDraw
file = open("input.txt", "r")

coor = []
fold = []
sheet = []

long=0
haut=0

def unique(arr):
    ret = []
    for a in arr:
        if a not in ret:
            ret.append(a)
    return ret

for line in file:
    if "," in line:
        temp = [int(x) for x in line.replace("\n","").split(',')]
        long = max(long,temp[0]+1)
        haut = max(haut,temp[1]+1)
        coor.append(temp)
    elif 'fold along ' in line:
        fold.append([line[11:12],int(line[13:])])

IMAGES = []
i = 0
f = fold[0]
tempcoor=[]
if f[0] == 'y' :
    haut = f[1]
    for c in coor:
        tempy = c[1]
        if tempy == f[1]:
            continue
        if c[1]>f[1]:
            tempy = 2*f[1]-c[1]
        tempcoor.append([c[0],tempy])
else:
    long = f[1]
    for c in coor:
        tempx = c[0]
        if tempx == (f[1]):
            continue
        if c[0] > f[1]:
            tempx = 2*f[1]-c[0]
        tempcoor.append([tempx, c[1]])
coor = unique(tempcoor)

TILE_SIZE = 5
out = Image.new("RGB", (TILE_SIZE * long, TILE_SIZE * haut), (0, 0, 0))
img = ImageDraw.Draw(out)
for y in range(haut):
    for x in range(long):
        if [x,y] in coor:
            color = (255, 100, 20)
        else:
            color = (50, 50, 50)
        shape = [(x * TILE_SIZE, y * TILE_SIZE), ((x + 1) * TILE_SIZE, (y + 1) * TILE_SIZE)]
        img.rectangle(shape, fill=color)
out.save("out.png")
out.show()
print(len(coor))