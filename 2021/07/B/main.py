from pprint import pprint

f = open("input.txt", "r")
crabs=[int(x) for x in f.readline().split(",")]
minfuel = 999999999999
for i in range(min(crabs),max(crabs)+1):
    temp = 0
    for c in crabs:
        dist = abs(c-i)
        temp += dist*(dist+1)/2
    minfuel = min(minfuel,temp)

print(minfuel)
