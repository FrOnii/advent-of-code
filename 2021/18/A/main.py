from pprint import pprint
from ast import literal_eval

def sf_template():
    ret = {}
    for t in [(c1, c2, c3, c4) for c1 in [0,1] for c2 in [0,1] for c3 in [0,1] for c4 in [0,1]]:
        ret[t]=None
    return ret

def tuple_to_int(t):
    total = 0
    for i in range(len(t)):
        if t[i] > 0:
            total += 2 ** i
    return total

def int_to_tuple(nb):
    t = ()
    puissance = DELTA * 4
    while puissance > 1 :
        puissance = puissance//2
        c = 0
        if nb >= puissance:
            c = 1
            nb = nb - puissance
        t = (c,)+t
    taille = 0
    while 2**taille < DELTA:
        taille+=1
    while len(t)<taille:
        t = t + (0,)
    return t


DELTA = 8
FIRST_TUPLE = int_to_tuple(DELTA*2)
def profondeur_max(arr):
    if not isinstance(arr,list):
        return 0
    ret = 0
    for p in arr:
        ret = max(ret,profondeur_max(p)+1)
    return ret

def list_to_sf(l,tuple=FIRST_TUPLE,delta=DELTA):
    if isinstance(l,int) or tuple[0]==1:
        if isinstance(l,list):
            l = [r for r in l if r is not None]
            if not l:
                l = None
            elif len(l) == 1:
                l = l[0]
        return {tuple: l}
    else:
        i = tuple_to_int(tuple)
        t1 = int_to_tuple(i - delta)
        t2 = int_to_tuple(i + delta)
        ret = list_to_sf(l[0],t1,delta//2) | list_to_sf(l[1],t2,delta//2)
        return  ret

def sf_to_list(sf,tuple=FIRST_TUPLE,delta=DELTA):
    if tuple in sf:
        return sf[tuple]
    elif tuple[0]==1:
        return None
    else:
        i = tuple_to_int(tuple)
        ret = [sf_to_list(sf,int_to_tuple(i-delta),delta//2),sf_to_list(sf,int_to_tuple(i+delta),delta//2)]
        return ret

def explode(sf,t,g,d):
    i = tuple_to_int(t)
    for j in range(i-1,-1,-1):
        temp_tuple = int_to_tuple(j)
        if temp_tuple in sf:
            if isinstance(sf[temp_tuple], int):
                sf[temp_tuple] += g
            else:
                sf[temp_tuple][1] += g
            break
    for j in range(i+1, DELTA * 4):
        temp_tuple = int_to_tuple(j)

        if temp_tuple in sf:
            if isinstance(sf[temp_tuple], int):
                sf[temp_tuple] += d
            else:
                sf[temp_tuple][0] += d
            break
    sf[t] = 0
    return sf

def is_reduced(sf):
    for k,v in sf.items():
        if isinstance(v,list):
            return False
        elif isinstance(v,int) and v >=10:
            return False
    return True

def reduce(sf):
    while not is_reduced(sf):
        print(sf_to_list(sf, int_to_tuple(DELTA * 2), DELTA))
        for key, node in sf.items():
            if not node:
                continue

            if isinstance(node,int):

                if node >= 10:
                    g,d = [node//2,node-node//2]
                    if key[0]==1:
                        sf=explode(sf,key,g,d)
                    else:
                        sf[key]=[g,d]
                        sf=list_to_sf(sf_to_list(sf))
            else:

                g,d=node.copy()
                sf = explode(sf,key,g,d)
    return sf

def add(a,b):
    ta = sf_to_list(a)
    tb = sf_to_list(b)
    return reduce(list_to_sf([ta,tb]))

f = open("input.txt", "r")
inp = [literal_eval(l.replace("\n","")) for l in f.readlines()]
template = sf_template()
sf = list_to_sf(inp[0])
inp = inp[1:]
while inp:
    print(sf_to_list(sf, int_to_tuple(DELTA * 2), DELTA))
    sf = add(sf,list_to_sf(inp[0]))
    inp = inp[1:]

print(sf_to_list(sf, int_to_tuple(DELTA * 2), DELTA))


