f = open("input.txt", "r")
totalX = 0
totalZ = 0
for line in f:
    line = line.replace("\n","")
    if(line[0:-2] == 'up'):
        totalZ = totalZ - int(line[-1])
    elif(line[0:-2] == 'down'):
        totalZ = totalZ + int(line[-1])
    elif(line[0:-2] == 'forward'):
        totalX = totalX + int(line[-1])

print(totalX*totalZ)
