f = open("input.txt", "r")
totalX = 0
totalZ = 0
aim = 0
for line in f:
    line = line.replace("\n","")
    if(line[0:-2] == 'up'):
        aim = aim - int(line[-1])
    elif(line[0:-2] == 'down'):
        aim = aim + int(line[-1])
    elif(line[0:-2] == 'forward'):
        totalX = totalX + int(line[-1])
        totalZ = totalZ + int(line[-1])*aim

print(totalX*totalZ)
