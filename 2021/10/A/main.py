f = open("input.txt", "r")
inp = f.read().split("\n")
CLOSECHAR={"{":"}","[":"]",'(':')','<':'>'}
CLOSECHARPOINTS={")":3,"]":57,"}":1197,">":25137}
OPENCHAR=CLOSECHAR.keys()

score = 0
for line in inp:
    open=[]
    i = 0
    while i < len(line):
        char = line[i]
        print(char)
        if char in OPENCHAR:
            open.append(char)
        else:
            if char == CLOSECHAR[open[-1]]:
                open = open[0:-1]
            else:
                score += CLOSECHARPOINTS[char]
                i = len(line)
        i += 1
print(score)
