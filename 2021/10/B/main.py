f = open("input.txt", "r")
inp = f.read().split("\n")
CLOSECHAR={"{":"}","[":"]",'(':')','<':'>'}
CLOSECHARPOINTS={")":1,"]":2,"}":3,">":4}
OPENCHAR=CLOSECHAR.keys()

scores = []
for line in inp:
    open=[]
    i = 0
    while i < len(line):
        char = line[i]
        if char in OPENCHAR:
            open.append(char)
        else:
            if char == CLOSECHAR[open[-1]]:
                open = open[0:-1]
            else:
                i = len(line)
        i += 1
    if i == len(line):
        score = 0
        for j in range(len(open)-1,-1,-1):
            score = score*5 + CLOSECHARPOINTS[CLOSECHAR[open[j]]]
        scores.append(score)

scores.sort()
print(scores[int(len(scores)/2-0.5)])
