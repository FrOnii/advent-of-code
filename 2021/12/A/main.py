def parcourir(path,start,history):
    if start == 'end':
        return 1
    nb = 0
    for suite in path[start]:
        if not (suite in history and suite==suite.lower()):
            nb += parcourir(path,suite,history+suite)
    return nb


f = open("input.txt", "r")

paths = {}

for line in f:
    start,end = line.replace("\n","").split("-")
    if not start in paths.keys():
        paths[start]=[]
    if not end in paths.keys():
        paths[end] = []
    if not end == 'start':
        paths[start].append(end)
    if not start == 'start':
        paths[end].append(start)

print(parcourir(paths,'start','start'))
