
SMALL = []
def check(cave,history):
    if cave == cave.lower() and cave in history:
        for s in SMALL:
            if history.count(s) > 1 :
                return False
    return True

def parcourir(path,start,history):
    temph = history.copy()
    temph.append(start)
    if start == 'end':
        return 1
    nb = 0
    for suite in path[start]:
        if check(suite,temph):
            nb += parcourir(path,suite,temph)
    return nb

f = open("input.txt", "r")

paths = {}

for line in f:
    start,end = line.replace("\n","").split("-")
    if not start in paths.keys():
        paths[start]=[]
    if not end in paths.keys():
        paths[end] = []
    if not end == 'start':
        paths[start].append(end)
    if not start == 'start':
        paths[end].append(start)

del paths['end']

for k in paths.keys():
    if k == k.lower():
        SMALL.append(k)

print(parcourir(paths,'start',[]))