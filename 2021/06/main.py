from pprint import pprint

f = open("input.txt", "r")
fishes = [int(x) for x in f.readline().split(",")]
days = 0
counters=[0,0,0,0,0,0,0,0,0]
for fish in fishes:
    counters[fish]+=1

print(counters)
while days < 256:
    temp = counters[0]
    counters = counters[1:] + [temp]
    counters[6] += temp
    days +=1

total=0
for i in counters:
    total+=i
print(total)
