f = open("input.txt", "r")
polymer = f.readline().replace("\n", "")
f.readline()
nbstep = 996
insert = {}
count_template = {}
already_calculated = {}

for line in f:
    insert[line[0:2]] = line[6]
    count_template[line[6]] = 0

def fonction(pair,profondeur,profondeurmax):
    if pair+str(profondeur) in already_calculated.keys():
        count=already_calculated[pair+str(profondeur)]
    else:
        count = count_template.copy()
        if profondeur == profondeurmax:
            count[pair[0]] += 1
        else:
            char = insert[pair]
            temp1 = fonction(pair[0]+char,profondeur+1,profondeurmax)
            temp2 = fonction(char+pair[1],profondeur+1,profondeurmax)
            for c in count.keys():
                count[c] += temp1[c] + temp2[c]
        already_calculated[pair+str(profondeur)]= count
    return count


out = count_template.copy()
for i in range(len(polymer)-1):
    pair = polymer[i]+polymer[i+1]
    if pair in already_calculated.keys():
        calcul = already_calculated[pair]
    else:
        calcul = fonction(pair,0,nbstep)
        already_calculated[pair+"0"] = calcul
    for c in out.keys():
        out[c] += calcul[c]

out[polymer[-1]]+=1
print(max(out.values())-min(out.values()))