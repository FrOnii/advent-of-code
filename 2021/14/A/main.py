f = open("input.txt", "r")
polymer =  f.readline().replace("\n","")
f.readline()
nbstep = 10
insert = {}
for line in f :
    insert[line[0:2]]=line[6]

for s in range(nbstep):
    temp = polymer[0]
    for i in range(1,len(polymer)):
        pair = polymer[i-1]+polymer[i]
        temp = temp + insert[pair] + polymer[i]
    polymer=temp

count=[]
for c in polymer:
    count.append(polymer.count(c))
diff = max(count)-min(count)
print(diff)