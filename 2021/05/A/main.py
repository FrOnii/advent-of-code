from pprint import pprint

f = open("input.txt", "r")
moves = []
tables = []
tablesCheck = []
maxX=0
maxY=0
for line in f:
    temp = line.replace("\n","").split(" -> ")
    [x1,y1]=temp[0].split(',')
    [x2,y2]=temp[1].split(',')
    maxX=max(max(int(x1),int(x2)),maxX)
    maxY=max(max(int(y1),int(y2)),maxY)
    moves.append([int(x1),int(y1),int(x2),int(y2)])

plateau = []
for i in range(maxY+1):
    temp=[0]*(maxX+1)
    plateau.append(temp)

for [x1,y1,x2,y2] in moves:
    if x1==x2:
        if y1>y2:
            r = range(y2,y1+1)
        else:
            r = range(y1,y2+1)
        for i in r:
            plateau[i][x1] +=1
    elif y1==y2:
        if x1>x2:
            r = range(x2,x1+1)
        else:
            r = range(x1,x2+1)
        for i in r:
            plateau[y1][i] +=1
    else:
        length=x2-x1
        idX=1
        if x1>x2:
            idX=-1
            length=-length
        idY=1
        if y1>y2:
            idY=-1
        for i in range(length+1):
            plateau[y1+idY*i][x1+idX*i] +=1
total=0
for a in plateau:
    for b in a:
        if b>1:
            total+=1
print(total)
