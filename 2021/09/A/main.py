f = open("input.txt", "r")
inp = f.read().split("\n")

def check(inp,x,y):
    numb = int(inp[y][x])
    checkUp = y > 0
    checkDown = y < len(inp) - 1
    lowSpot = True
    if x > 0:
        lowSpot = lowSpot and (int(inp[y][x - 1]) > numb)
    if lowSpot and x < len(inp[y]) - 1:
        lowSpot = lowSpot and (int(inp[y][x + 1]) > numb)
    if lowSpot and checkUp:
        lowSpot = lowSpot and (int(inp[y - 1][x]) > numb)
    if lowSpot and checkDown:
        lowSpot = lowSpot and (int(inp[y + 1][x]) > numb)
    return lowSpot

total = 0
for i in range(len(inp)):
    y = int(i)

    for j in range(len(inp[y])):
        numb = int(inp[int(j)][int(i)])
        if check(inp,int(i),int(j)):
            total += numb + 1

print(total)