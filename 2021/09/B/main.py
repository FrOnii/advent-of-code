from PIL import Image, ImageDraw
from random import randint

f = open("input.txt", "r")
file = f.read().split("\n")
ID = 0
cratere = []
inp = []
for a in file:
    inp.append([int(x) for x in a])
    cratere.append([0 for x in a])


def checkCase(inp, x, y):
    if x < 0 or y < 0 or y >= len(inp) or x >= len(inp[y]):
        return False
    numb = int(inp[y][x])
    checkUp = y > 0
    checkDown = y < len(inp) - 1
    lowSpot = True
    if x > 0:
        lowSpot = lowSpot and (int(inp[y][x - 1]) > numb)
    if lowSpot and x < len(inp[y]) - 1:
        lowSpot = lowSpot and (int(inp[y][x + 1]) > numb)
    if lowSpot and checkUp:
        lowSpot = lowSpot and (int(inp[y - 1][x]) > numb)
    if lowSpot and checkDown:
        lowSpot = lowSpot and (int(inp[y + 1][x]) > numb)
    return lowSpot


def sizeZone(inp, x, y):
    size_ret = 0
    if inp[y][x] < 9:
        cratere[y][x] = ID
        size_ret += 1
        temp = inp
        temp[y][x] = 9
        if x > 0:
            size_ret += sizeZone(temp, x - 1, y)
        if x < len(inp[y]) - 1:
            size_ret += sizeZone(temp, x + 1, y)
        if y > 0:
            size_ret += sizeZone(temp, x, y - 1)
        if y < len(inp) - 1:
            size_ret += sizeZone(temp, x, y + 1)

    return size_ret


total = 1
maxsizearray = [0, 0, 0]
for y in range(len(inp)):
    for x in range(len(inp[y])):
        if checkCase(inp, x, y) and inp[y][x] < 9:
            ID += 1
            size = sizeZone(inp, x, y)
            maxsizearray.append(size)
            maxsizearray.sort()
            maxsizearray = maxsizearray[1:]

print(maxsizearray[0], maxsizearray[1], maxsizearray[2], maxsizearray[0] * maxsizearray[1] * maxsizearray[2])

# BONUS

blocksize = 10
out = Image.new("RGB", (len(inp[0]) * blocksize, len(inp) * blocksize), (255, 255, 255))
img = ImageDraw.Draw(out)
randomColors = [(0, 0, 0)]
for i in range(ID):
    randomColors.append((randint(10, 85) * 3, randint(10, 85) * 3, randint(10, 85) * 3))
for y in range(len(cratere)):
    for x in range(len(cratere[y])):
        shape = [(x * blocksize, y * blocksize), ((x + 1) * blocksize, (y + 1) * blocksize)]
        img.rectangle(shape, fill=randomColors[cratere[y][x]])

out.show()
