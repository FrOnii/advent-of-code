f = open("input.txt", "r")

def clear(arr,bit,position):
    ret = []
    for a in arr:
        if int(a[position])==bit:
            ret.append(a)
    return ret

def getMoreCommon(arr,position):
    total = 0
    for a in arr:
        total = total + int(a[position])
    if total >= len(arr)/2:
        return 1
    else:
        return 0

def getLeastCommon(arr,position):
    total = 0
    for a in arr:
        total = total + int(a[position])
    if total >= len(arr)/2:
        return 0
    else:
        return 1

def strbitToInt(str):
    rev = str[::-1]
    tot = 0
    for c in range(len(rev)):
        tot = tot + int(rev[c])*2**c
    return tot

tab = f.read().split('\n')
lenLine = len(tab[0])
oxyTab = tab
carTab = tab
oxygen =0
carbon = 0

for i in range(lenLine):
    
    
    if len(oxyTab)>1:
        commonO = getMoreCommon(oxyTab,i)
        oxyTab = clear(oxyTab,commonO,i)

    if len(carTab)>1:
        leastC = getLeastCommon(carTab,i)
        carTab = clear(carTab,leastC,i)

oxygen = strbitToInt(oxyTab[0])
carbon = strbitToInt(carTab[0])

print(oxygen*carbon)
