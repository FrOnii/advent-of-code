f = open("input.txt", "r")
nbLines =0
firstLine = f.readline().replace("\n","")
nbOne = [int(b) for b in firstLine]
lenLine = len(nbOne)
for line in f:
    nbLines = nbLines + 1
    for i in range(lenLine):
        nbOne[i] =  nbOne[i] + int(line[i])
gamma=0
epsilon=0

for i in range(lenLine-1,-1,-1):
    if nbOne[i] > int(nbLines/2):
        gamma = gamma + 2**(lenLine-1-i)
    else:
        epsilon = epsilon + 2**(lenLine-1-i)
        
print(gamma*epsilon)
