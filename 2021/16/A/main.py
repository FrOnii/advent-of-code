def hexToBin(str):
    dict = {"0": "0000", "1": "0001", "2": "0010", "3": "0011", "4": "0100", "5": "0101", "6": "0110", "7": "0111",
            "8": "1000", "9": "1001", "A": "1010", "B": "1011", "C": "1100", "D": "1101", "E": "1110", "F": "1111"}
    ret = ""
    for c in str:
        ret = ret + dict[c]
    return ret

def prod(arr):
    p = 1
    for i in arr:
        p *= i
    return p

def binToInt(str):
    total = 0
    for i in range(len(str) - 1, -1, -1):
        if int(str[i]) > 0:
            total += 2 ** (len(str) - 1 - i)
    return total


VS = 0
def add_version_sum(i):
    global VS
    VS += i

def parse(packet):
    version = binToInt(packet[0:3])
    add_version_sum(version)
    id = binToInt(packet[3:6])
    content = packet[6:]
    value_bin = ""
    #literal value
    if id==4:
        for i in range(0, len(content), 5):
            value_bin = value_bin + (content[i + 1:i + 5])
            if content[i] == "0":
                value = binToInt(value_bin)
                new_content = content[i + 5:]
                return value, new_content
    length_id = content[0]
    content = content[1:]
    temp_rets=[]
    if length_id == "0":
        length = binToInt(content[:15])
        content = content[15:]
        sub = content[:length]
        content = content[length:]
        while sub:
            ret, sub = parse(sub)
            temp_rets.append(ret)
    else:
        nb_sub = binToInt(content[:11])
        content = content[11:]
        for i in range(nb_sub):
            ret, content = parse(content)
            temp_rets.append(ret)
    return ret, content

f = open("input.txt", "r").readline()
print(parse(hexToBin(f)))
print(VS)