f = open("input.txt", "r")
last = int(f.readline())
total = 0
for line in f:
    temp = int(line)
    if last < temp:
        total = total + 1
    last = temp
print(total)
