import sys

def sum(arr):
	s = 0
	for i in arr:
		s = s + int(i)
	return s

def run1(input):
	print(max([sum(x.split("\n")[:-1]) for x in input.split("\n\n")]))

def run2(input):
	pass

input = open(sys.argv[1]).read()
if sys.argv[2] =='1':
	run1(input)
elif sys.argv[2] == '2':
	run2(input)
else:
	print("Choisis 1 ou 2")
